---
vim: et ts=2 sts=2 sw=2
title: installation sympa arn
keywords: opensmtpd sympa chaton
author: Marc Chantreux <marc.chantreux@renater.fr>
---

# liens externes

* [someone wants to install on openbsd](https://www.mail-archive.com/misc@openbsd.org/msg163966.html)
* [koolfy](https://github.com/sympa-community/sympa/issues/32)

# install

## sympa-deps

pour créer le paquet

    cd devtools
    equivs-build sympa-deps-control

installer le paquet

    dpkg --install sympa-deps_1.1_amd64.deb
    aptitude install

## 



