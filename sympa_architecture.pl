package List;
use Sympatic;
use Types::Standard qw( ArrayRef InstanceOf );

has qw( sources is rw )
, isa => InstanceOf['Source'];

package Source;

package User;

package Media::Id::User::Mail;
use Sympatic;
use Types::Standard qw( ArrayRef InstanceOf );

has qw( user is rw )
, isa => InstanceOf['User'];

package main;
use Sympatic -oo;
