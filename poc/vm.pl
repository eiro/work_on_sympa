use Test::More;

package Batman {
    use Sympatic;
    has qw( moto is rw );
    method sing () { "nanananananana ..." }
}

package VM {

    use Sympatic;

    sub normalize {
        my $self = shift;
        map {
            if (my $r = ref $_ ) {
                if ( $r eq 'ARRAY') { $self->exec(@$_) }
                else {$_}
            } else { $_ }
        } @_
    }

    has qw( scratchpad is rw ) , default => sub { +{} };
    has qw( fn         is rw ) , default => sub { +{} };

    method set  ( $key, $value ) { $self->scratchpad->{$key} = $value }
    method get  ( $key )         { $self->scratchpad->{$key} }
    method call ( $fn, @params)  { $self->fn->{$fn}->( $self->normalize(@params) ) }
    method send ( @params ) {
        my ( $obj, $msg );
        ( $obj, $msg, @params ) = $self->normalize(@params);
        $obj->$msg(@params)
    }

    method exec ($op, @params)  {
        grep {$op eq $_} qw( if ) and return $self->$op( @params );
        $self->$op( $self->normalize(@params) );
    }

    method run (@instructions) { $self->exec(@$_) for @instructions; }

    method if ( $condition, $true, $false ) {
        my ($r)   = $self->normalize( $condition );
        if ( $r ) { $self->normalize($true)  }
        else      { $self->normalize($false) }
    }

};

use Sympatic -oo;

my $vm = VM->new (
    fn => {
        dump =>  sub { use YAML ();  say YAML::Dump [@_] },
        sum  =>  sub {
            my $sum=0;
            $sum+=$_ for @_;
            $sum;
        },
        answer => sub { my $sum=0; $sum+=$_ for @$_; $sum },
        better => sub { $_[0] eq 'foo' }
    },
);

my $s = $vm->scratchpad;

$vm->run(

    [qw( set x 12 )],
    [qw( set y 15 )],
    [qw( set z ),
        [ qw( call sum )
        , [qw( get x )]
        , [qw( get y )] ] ]

);

is $$s{x}      => 12, 'can affect a variable';
is $$s{y}      => 15, 'even twice';
is $$s{z}      => (12 + 15 ), 'call sum works';

$vm->run(

    [ if =>
        [qw( call better foo bar )]
        , [qw( set winner Chuck  )]
        , [qw( set winner Norris )] ],

    [ if =>
        [qw( call better bar bang )]
        , [qw( set loser Donald  )]
        , [qw( set loser Trump )] ]

);


is $$s{winner} => 'Chuck', 'Chuck always wins';
is $$s{loser}  => 'Trump', 'Trump is a loser';

$vm->run(
    [qw( set batman ) => [ qw( send Batman new moto YES ) ] ],
    [qw( set moto   ) =>
        [ send => [qw( get batman)], qw( moto ) ]
    ]
);

isa_ok $$s{batman} , 'Batman' => 'send Batman new';
is     $$s{moto}   , 'YES'    => 'call the moto';

done_testing;
