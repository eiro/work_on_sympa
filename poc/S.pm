package S;
use Sympatic;
use Test::More;
use YAML ();
use JSON;

sub tokens {
    grep /\S/
    , split /
    ( # here come the tokens
    | \(\#
    | \(
    | \)
    | \"(?: \\.|[^"])*\"
    | [^\s)]+
    ) /xms, shift;
};

sub remove_comments {
    my $plvl = 0;
    grep {
        if ($plvl) {
            # parentheses are ok in comments if they are paired
            if ( '(' eq $_ ) { ++$plvl }
            if ( ')' eq $_ ) { --$plvl }
            0;
        } else {
            # comment intro
            if ('(#' eq $_) { $plvl++ }
            else { 1 }
        }
    } @_;
}

sub ast {
    my @ast;
    my @stack;
    my $current = \@ast;
    my $previous;

    for (@_) {
        if ( $_ eq '(') {
            push @stack, $previous = $current;
            push @$previous, $current = [];
        } elsif ( $_ eq ')') { $current = pop @stack }
        else { push @$current, $_ }

    }

    \@ast;
}

sub parse { ast remove_comments tokens @_ }

1;

__END__

# no warnings; # TODO: warnings what ?

my @tokens = tokens q!
    (wow)
    (# no comment (parcequ'eux) )
    (glob $sender.domain *.fr)
!;

is_deeply \@tokens, [qw!
    ( wow )
    (# no comment ( parcequ'eux ) )
    ( glob $sender.domain *.fr )
!]  => "tokens comments";

is_deeply
    [@tokens = remove_comments @tokens]
    , [qw!
        ( wow )
        ( glob $sender.domain *.fr ) !]
    , "remove comments"
        or diag join ",", @tokens;

sub clean_ast { ast remove_comments tokens shift }

note "example 1\n" => YAML::Dump clean_ast q!

    (# le nouveau format pour les scenarii
        c'est un chouette exemple d'utilisation des macros
        "avec du style" et des parenthèses (et tout) ...)

    (if (and +auth (glob *.fr $sender.domain )) (accept))
    (msg-
        "you must be authenticated with an adress from
        a french domain to play this scenario")
    (reject)

!;

note "example 2\n" => YAML::Dump clean_ast q/
     (when
         (# quand c'est pas quelqu'un de chez nous,
            on modere si c'est un admin. sinon on rembare)
         ((!= sender:domain renater.fr)
             (if (= sender:user admin)
                 (editorkey)
                 (reject (msg "not (very) welcome, #{sender:user}"))))
/;

note "example reporting\n" => encode_json clean_ast q/

(when (any context smtp dkim smime md5)
  (when (sender.is_editor? (listname))
    (accept)
    (reject :reason send_editor)))
/;

done_testing;
