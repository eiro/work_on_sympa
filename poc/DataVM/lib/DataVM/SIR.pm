package DataVM::SIR;
use strict;
use warnings;

sub tokens {
    grep /\S/
    , split /
    ( # here come the tokens
    | \(\#
    | \(
    | \)
    | \"(?: \\.|[^"])*\"
    | [^\s)]+
    ) /xms, shift;
};

sub remove_comments {
    my $plvl = 0;
    grep {
        if ($plvl) {
            # parentheses are ok in comments if they are paired
            if ( '(' eq $_ ) { ++$plvl }
            if ( ')' eq $_ ) { --$plvl }
            0;
        } else {
            # comment intro
            if ('(#' eq $_) { $plvl++ }
            else { 1 }
        }
    } @_;
}

sub raw_ast {
    my @ast;
    my @stack;
    my $current = \@ast;
    my $previous;

    for (@_) {
        if ( $_ eq '(') {
            push @stack, $previous = $current;
            push @$previous, $current = [];
        } elsif ( $_ eq ')') { $current = pop @stack }
        else { push @$current, $_ }

    }

    \@ast;
}

sub ast { raw_ast remove_comments tokens shift }

1;


