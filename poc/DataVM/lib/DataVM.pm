package DataVM;
use Sympatic;

sub normalize {
    my $self = shift;
    map {
        if (my $r = ref $_ ) {
            if ( $r eq 'ARRAY') { $self->exec(@$_) }
            else {$_}
        } else { $_ }
    } @_
}

has qw( scratchpad is rw ) , default => sub { +{} };
has qw( fn         is rw ) , default => sub { +{} };
has qw( raw        is rw ) , default => sub {
    +{map +( $_ => 1 ), qw( if while ) }
};

method set  ( $key, $value ) { $self->scratchpad->{$key} = $value }
method get  ( $key )         { $self->scratchpad->{$key} }
method call ( $fn, @params)  { $self->fn->{$fn}->( $self->normalize(@params) ) }
method send ( @params ) {
    my ( $obj, $msg );
    ( $obj, $msg, @params ) = $self->normalize(@params);
    $obj->$msg(@params)
}

method exec ($op, @params)  {

    $op = do {
        if ( ref $op ) { $op }
        else {
            $self->raw->{$op}
                or @params = $self->normalize(@params);
            $self->fn->{$op}
            //= $self->can($op)
            // die "can't call $op operator"
        }
    };

    $self->$op( @params );

}

method run (@instructions) {
    my @r;
    for (@instructions) {
        @r = $self->exec(@$_)
    };
    wantarray ? @r : shift @r;
}

method if ( $condition, $true, $false ) {
    my ($r)   = $self->normalize( $condition );
    if ( $r ) { $self->normalize($true)  }
    else      { $self->normalize($false) }
};

# TODO: the rest of the file must be a role ?

use YAML ();

method debug ( $prg ) {
    for my $lnum ( 0..@$prg-1 ) {
        my $lset = $$prg[$lnum];
        say STDERR YAML::Dump { "line $lnum" => $lset };
        $self->exec( @$lset );
        say STDERR YAML::Dump { "scratchpad" => $self->scratchpad };
    }
};

method try ( $prg ) {
    for my $lnum ( 0..@$prg-1 ) {
        my $lset = $$prg[$lnum];
        eval { $self->exec( @$lset ) };
        if ( $@ ) { die YAML::Dump { error => $@,  "line $lnum" => $lset }  }
    }
};

1;
