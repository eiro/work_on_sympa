use lib qw( lib );
use DataVM;
use DataVM::SIR;
use Sympatic;
use Test::More;

my $vm = DataVM->new(
    fn =>
    { down    => method () {
        $self->scratchpad->{x}--
    }
    , upgrade => method () {
        $self->scratchpad->{y} += 10
    }
    , while => method ( $condition, $action ) {
            my @r;
            while ( $self->run( $condition ) ) {
                @r = $self->run( $action )
            }
            @r
        }
    }
);

my $prg = DataVM::SIR::ast(q!
    (set x 3)
    (set y 0)
    (while (down) (upgrade))
!);

$vm->try( $prg );
is $vm->scratchpad->{y}, 30, 'while loop works';

done_testing;
