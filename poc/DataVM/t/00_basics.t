use Sympatic -oo;
use Test::More;
use lib qw( lib );
use_ok 'DataVM';
use_ok 'DataVM::SIR';

package Batman {
    use Sympatic;
    has qw( moto is rw );
    method sing () { "nanananananana ..." }
}

sub new_vm_to_test {
    DataVM->new(
        fn => {
            dump =>  sub { use YAML ();  say YAML::Dump [@_] },
            sum  =>  sub {
                my $sum=0;
                $sum+=$_ for @_;
                $sum;
            },
            answer => sub { my $sum=0; $sum+=$_ for @$_; $sum },
            better => sub { $_[0] eq 'foo' },
            true   => sub { 1 },
            false  => sub { 0 },
        },
    );
}

my $vm = new_vm_to_test;

fun test_scratchpad_state ( $vm ) {

    my $s = $vm->scratchpad;

    is $$s{x}      => 12, 'can affect a variable';
    is $$s{y}      => 15, 'even twice';
    is $$s{z}      => (12 + 15 ), 'can call a function';
    is $$s{winner} , Chuck => 'if: true  block honored';
    is $$s{loser}  , Trump => 'if: false block honored';
    isa_ok $$s{batman} , 'Batman' => 'send Batman new';
    is     $$s{moto}   , 'YES'    => 'call the moto';

}

isa_ok $vm, DataVM => "vm constructor ok";

$vm->run(

    [qw( set x 12 )],
    [qw( set y 15 )],
    [qw( set z ),
        [ qw( call sum )
        , [qw( get x )]
        , [qw( get y )] ] ],
    [ if =>
        [qw( call better foo bar )]
        , [qw( set winner Chuck  )]
        , [qw( set winner Norris )] ],
    [ if =>
        [qw( call better bar bang )]
        , [qw( set loser Donald  )]
        , [qw( set loser Trump )] ],
    [qw( set batman ) => [ qw( send Batman new moto YES ) ] ],
    [qw( set moto   ) =>
        [ send => [qw( get batman)], qw( moto ) ] ]

);

test_scratchpad_state $vm;
note "now parsing a SIR";

$vm = new_vm_to_test;
my $prg = DataVM::SIR::ast(q!
    (set x 12)
    (set y 15)
    (set z (call sum (get x) (get y)))
    (if (call better foo bar)
        (set winner Chuck)
        (set winner Norris))
    (if (call better bar bang)
        (set loser Donald)
        (set loser Trump))
    (set batman (send Batman new moto YES ))
    (set moto (send (get batman) moto))
    (set moto (send (get batman) moto )))
!);

$vm->try( $prg );
test_scratchpad_state $vm;

ok $vm->exec('true')           => 'true is true';
ok +( not $vm->exec('false') ) => 'false is false';

ok +(
    $vm->exec( @{ DataVM::SIR::ast(q!true!) } )
) => 'SIR true is true';
ok +(
    not $vm->run( @{  DataVM::SIR::ast(q!(false)!) } )
) => 'SIR false is false';

done_testing;
