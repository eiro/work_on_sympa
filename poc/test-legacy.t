# use Sympatic -oo;
use lib '.';
use legacy;
use YAML;
use Data::Dumper;
use Test::More;

# the goal is to parse a scenario

my $complete_scenario = <<'EOS';
# no comment ...
you(should,[not], /pass/)
    dkim,smtp -> reject(reason=gandalf.tt2),log

# still: no comment
no_way(reveal([sender])) scp,udp -> make_pun
EOS

# objectif
#    (if (and
#            (context (q dkim smtp))
#            (ok (q 34) (SV sender) (true (q john 7))))
#        (do (reject (q bareword reason=goo.tt2))
#            (do_it))
#        (if (and
#                (context (q scp udp))
#                (wow (q 34) (SV sender) (q john)))
#            (love_it))

my $no_comment = <<'EOS';
    ok( 34 , [sender] , true(john,7) )
        dkim,smtp -> reject(reason=goo.tt2),do_it
    wow( 34 , [sender] , true(john,7) )
        scp,udp -> love_it
EOS

# print Dumper legacy::parse *legacy::scenario for $no_comment;

sub test_rule {

    my ( $name, $desc, $rule, $should, $shouldnt ) = @$_;
    my ( $expected, $why);
    note "testing $desc";

    for ( @$should ) {
        ( $_, $expected, $why ) = @$_;
        my ($got) = legacy::parse $rule;
        is_deeply $got, $expected, "$_ : $why"
            or note YAML::Dump +{ match => $got };
    }

    for ( @$shouldnt ) {
        ( $_, $why ) = @$_;
        my ($got) = legacy::parse $rule;
        ok +( (not $got) || / \G (?<remains> .+ )/cgx ) => qq(avoid "$_" because $why);
    }

}

my $becomes =
[ [ # rule 1:
    # ok(...
    [ 'fcall', 'ok', [
    [ 'num', '34' ],
    [ 'variable', 'sender' ],
    [ 'fcall', 'true', [
        [ 'bareword', 'john' ],
        [ 'num', '7' ] ] ] ] ],
    # Contexte
    [ 'dkim', 'smtp' ],
    # actions
    [ [ 'fcall', 'reject', [ [ 'bareword', 'reason=goo.tt2' ] ] ],
      [ 'bareword', 'do_it' ] ] ],
    # rule 2:
    # now(...
      [ [ 'fcall', 'wow', [
        [ 'num', '34' ],
        [ 'variable', 'sender' ],
        [ 'fcall', 'true',
          [ [ 'bareword', 'john' ],
            [ 'num', '7' ] ] ] ] ],
    # contexte
    [ 'scp', 'udp' ],
    # actions
    [ [ 'bareword', 'love_it' ] ] ] ];

my $comment = '# this is it
# this one

    # this
#

XXX';

my $comment_but_meta = '# this is it
# this one

XXX

    # this
#

';


test_rule for

    [ scenario => "a complete scenario" , *legacy::scenario =>
        # , [ [ $complete_scenario => $becomes ] ]
    , [ [ $complete_scenario => $becomes => "yes we can" ] ]
    , []
    ],

    # [ comments => "comments" , sub {
    #         legacy::comments;
    #         /\G(.*)/xmsgc or return;
    #         # TODO: rewrite the test suite the way
    #         # you can return a bad value
    #         note YAML::Dump { rest => $1 };
    #         $1;
    #     }
    # =>  [ [ $comment          => 'XXX' => "yes we can" ]
    #     , [ # nan mais en fait ca marche :)
    #         $comment_but_meta => 'XXX' => "yes we can" ] ]
    #     , []
    # ],

; done_testing
__END__



; done_testing

    [ num => "find a numeric value" , *legacy::num =>
    # should
    [ [ '234' => [qw( num 234 )] , "simple number" ]
    , [ '1_0' => [qw( num 1_0 )] , "with _ separator" ]
    , [ '1.0' => [qw( num 1.0 )] , "floating with both lhs and rhs" ]
    , [ '.1'  => [qw( num .1  )] , "floating without lhs" ]
    , [ '2.'  => [qw( num 2.  )] , "floating without rhs" ]
    , [ '+10' => [qw( num +10 )] , "explicit positive number" ]
    , [ '-10' => [qw( num -10 )] , "negative number" ]
    ],
    # should not
    [ [ '1__0'     => "two _ in a row" ]
    , [ '+_234'    => "_ at the top (signed with +)" ]
    , [ '-_234'    => "_ at the top (signed with -)" ]
    , [ '_234'     => "_ at the top" ]
    # TODO: fix this :)
    # , [ '234_'     => "_ at the bottom" ]
    , [ '1..0'     => "too many comma (not a range)" ]
    , [ '1.0.0.0'  => "too many comma (not an IP adress)" ]
    , [ '1.0.'     => "too many comma (not even leading one)" ]
    , [ '.'        => "just ." ]
    ] ],

    [ var => "expect a variable expansion" , *legacy::variable =>
    # should
    [ [ '[this]' => [qw( variable this )] , "this" ]
    , ],
    # should not
    [ ],
    ],

    [ regexp => "regexp pattern" , *legacy::regexp =>
    # should
    [ [ '/this/' => [qw( regexp this )]  ]
    , ],
    # should not
    [ ],
    ],


    [ fcall => "function call" , *legacy::fcall =>
    # should
    [ [ 'true()' => [fcall => 'true'] ]
    , [ 'add(1,2)' =>
            [qw(fcall add ) =>
                [ [num => 1]
                , [num => 2] ] ] ]
    , [ 'add(1., 2)' =>
            [qw(fcall add ) =>
                [ [num => '1.']
                , [num => 2] ] ] ]
    , [ 'is_fine
        ( 34
        , [sender]
        , true(john,7)
        , j()
        , /hack/
        , wow
        , 56
        , 3 ) ' =>
            [qw( fcall is_fine ) =>
            [ [qw( num      34 )]
            , [qw( variable sender )]
            , [qw( fcall true ) =>
                    [ [qw( bareword john )]
                    , [qw( num      7    )] ] ]
            , [qw( fcall  j )]
            , [qw( regexp hack )]
            , [qw( bareword wow )]
            , [qw( num      56 )]
            , [qw( num       3 )] ] ]
        , "can parse is_analog" ], ], ],

__END__


