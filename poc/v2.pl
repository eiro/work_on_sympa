package VM;
use Sympatic;

package main;
use Sympatic -oo;

=head1 experiment v2

this is a V2 alternative, still no optimisation but more constistency

     * namespaces to store the methods
     * every call is encapsulated

caveat: some symbols are unable (maybe only one: C<'>);

=cut

{ my %fn =
        ( 'SV=' => method (@args) {
                my ($k, $v ) = $self->eval(@args);
                $$self{$k} = $v;
            }
        , eval  => method (@args) { map $self->call(@$_),@args }
        , 'SV'  => method ($k) { $$self{$k} }
        , 'SV--'  => method ($k) { $$self{$k}-- }
        , '--SV'  => method ($k) { --$$self{$k} }
        , 'SV~='  => method ($k,$v) { $$self{$k}.= $v }
        , printf  => method (@args) {
            printf $self->eval(@args);
        }
        , '<'     => method (@args) {
                my ($x,$y) = $self->eval(@args);
                $x < $y
            }
        , "q"   => sub { shift; @_ }
        , "'"   => sub { splice @_,0,2; @_ }
        , '+'   => method (@xs) {
            my $sum = 0;
            $sum+=$_ for $self->eval(@xs);
            $sum
        }
        , 'AV=' => method ($k, @list) {
                $$self{$k} = [ $self->eval(@list) ]
            }
        , 'AVpush' => method ($k, @list) {
                push
                @{ $$self{$k} // die "$k doesn't exist" }
                , $self->eval(@list);
            }
        , 'AV'  => method ($k, @pos) {
                if (@pos) { @{ $$self{$k} } }
                else      { $$self{$k}[@pos] } }
        , do    => method ($script) {
                my @r;
                for (@$script) {
                    @r = $self->call(@$_);
                }
                @r;
            }
        , call => method ($call, @args) { $self->$call(@args) }
        , while   => method ($if, $then) {
                while (($self->eval($if))[0]) { $self->eval($then) }
            }
        , if   => method ($if, $then, $else=[] ) {
                (($self->eval($if))[0])
                ? $self->eval($then)
                : $self->eval($else)
            }
        );
    use Sub::Install;
    Sub::Install::install_sub {
        into => 'VM'
        , as   => $_
        , code => $fn{$_} }
    for keys %fn;
};

use YAML;
use lib '.';
use S;
my $vm = new VM;
# x
say for $vm->do([[qw( q x )]]);
# x
say $vm->do( S::parse q!(q x)! );
# 3
say $vm->do( S::parse q!(+ (q 3))! );
# 3
say $vm->do( S::parse q!(SV= (q x) (+ (q 3)) ) ! );
# 4
say $vm->do( S::parse q!(SV= (q y 4) ! );
# 4
say $vm->do( S::parse q!(SV= (q y) (q 4) ! );
# 4
say $vm->do( S::parse q!(SV y) ! );
# 3
say $vm->do( S::parse q!(SV x) ! );

# x: 1
# y: 3
# z: 7


$vm = new VM;
$vm->do(
    [ ['SV=', ['q', 'x'], ['q', 12] ]
    , ['SV=', ['q', 'y'], ['q', 24] ]
    , ['SV=', ['q', 'z'], ['+', ['SV','x'], ['SV','y'] ] ] ] );

say $vm->do( S::parse q!
    (SV= (q x 1))
    (SV= (q y) (+ (q 3) )
    (SV= (q z) (+ (q 3) (SV y))))
! );
# say YAML::Dump $vm;

$vm->do( S::parse q!(AV= lista ) ! );
say $vm->do( S::parse q!(AVpush lista (q this is almost done)) ! );

# should get the reference of lista ?
# say $vm->do( S::parse q!(SV (q lista))! );

say $vm->do( S::parse q!
    (if (q 0)
        (SV= (q x) (q ok))
        (SV= (q x) (q ko))
    )
!);
# ko

say $vm->do( S::parse q!
    (if (q 1)
        (SV= (q x) (q ok))
        (SV= (q x) (q ko))
    )
!);
# ok

say YAML::Dump $vm;
say $vm->do( S::parse q!
    (SV= (q x) (q 10))
    (SV= (q fmt) (q x=%s\n) )
    (while (SV-- x)
        (printf (SV fmt) (SV x)
        )
!);
say YAML::Dump $vm;

S::parse q!
    (let x 1)
    (let y 3)
    (let z (+ 3 x))!;

__END__

# real script

(let x 1)
(let p (' 3 4 x 2))
(let z (* 2 (+ x p)))

# IR: function names inspired but man perlguts

# my $script =<<'E';
# (let x 4)
# (let y 3)
# (let z (+ x y))
# (let names (' this is it))
# (let banner)
# (while (!= 0 (-- z))
#     (do
#         (if
#             (defined (let n (aget names 3)))
#                 (~= banner " " n)))
#         (noop)))
# E
# 
# my $script =<<'E';
# (let x 4)
# (let y 3)
# (let z (+ x y))
# (let names (' this is it))
# (let banner)
# (while (!= 0 (-- z))
#     (do
#         (if
#             (defined (let n (aget names 3)))
#                 (~= banner " " n)))
#         (noop)))
# E
# 
# use YAML ();
# my $program = S::parse $script;
# 
# my %fn =
#     ( do     => method ($program) { $self->eval($_) for @$prg }
#     , eval   => method ($instruction) {
#     }
#     );
# #     [ if =>
# #         [qw( call better bar bang )]
# #         , [qw( set loser Donald  )]
# #         , [qw( set loser Trump )] ]
