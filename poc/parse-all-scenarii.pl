use Sympatic -oo;
use Path::Tiny;
use lib '.';
use S;
use legacy;
use YAML;

fun lines_of ( $text ) {
    my @lines;
    my $t = 0;
    my $l;
    push @lines, do {
        $l = length $&;
        [$l, ($t+= $l), $1]
    } while $text =~ /(.*)(?:\n|$)/mg;
    \@lines;
}

fun render_error ( $pos, $error, $lines ) {
    my $done = 0;
    join "\n", map {
        if ($done) { $$_[2] }
        else {
            if ( $pos < $$_[1] ) {
                $done=1;
                # TODO:easy
                # avoid warning message
                # Negative repeat count does nothing at ./parse-all-scenarii.pl line 27
                my $msg = '-' x ((length $$_[2] ) - $$_[1] + $pos);
                $$_[2], "$msg^\n    error: $error";
            }
            else { $$_[2] }
        }
    } @$lines;
}

sub show {
    my $filename = $_;
    $_ = path($filename)->slurp;
    my @lines = split "\n";
    my $state = eval { legacy::parse *legacy::scenario };
    $@ and do { say $@; exit };

    if ( $$state{fatal} ) {
        say "while parsing $filename: "
        , render_error @{ $$state{fatal} }
        , lines_of $_;
        exit;
    } else { say "$filename ok" }
}

# show for <Scenari-Loader-LegacySyntax/t/scenari/*>;

sub token_ir (_);
sub token_ir (_) {
    # exit if say YAML::Dump shift;
    my $in = shift;
    my ( $type, $arg, @args ) = @{$in};
    if    ($type eq 'fcall'    ) {
        @args
        ? [$arg => map token_ir, @{$args[0]} ]
        : [$arg]
    }
    elsif ($type eq 'bareword' ) { [q  => $arg ] }
    elsif ($type eq 'scalar'   ) { [SV => $arg ] }
    else {
        die "don't know what to do with : "
        , YAML::Dump @_
    }
}

sub rule_ir (_) {
    my ( $condition, $context, $actions ) = @{shift,};
    [if =>
        [and =>
            (token_ir $condition),
            ['any_context' => [q => @$context ] ] ],
        [do => map token_ir, @$actions ] ]
}

fun scenario_to_vm ( $data ) {
    my ( $meta, $ast ) = @$data;
    # is there an init phase ?

    my %init = (
        defined $meta
        ? ( _meta => +{ map @$_, @$meta } )
        : ()
    );

    my @rules = map rule_ir, reverse @$ast;
    # say Dump $rules[1];

}

my $file   = 'Scenari-Loader-LegacySyntax/t/scenari/subscribe.auth';
my $text   = path($file)->slurp;
my $parsed = legacy::parse *legacy::scenario, $text;
say YAML::Dump $parsed;
say "BECOMES";
say YAML::Dump scenario_to_vm $$parsed{data};
