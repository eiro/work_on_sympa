package legacy;
# use Test::More;
# use YAML;
use Sympatic -oo;
use re '/xms';

our %STATE;

my @vals;

sub fatal {
    $STATE{fatal} = [ pos , @_ ];
    goto STOP_PARSING;
}

sub remains { /\G (.*)/gc }

sub fcall;

sub num {

    # <decimals> matches anything starting with .
    # (like '.234' )
    # <float> matches anything else :)

    return unless m{
        \G \s* ( [-+]?  (?: (?&decimals) | (?&float) ) )
        (?(DEFINE)
            # digits can be separated with _
            (?<digit>                             \d_? )
            (?<decimals>                 \. (?&digit)+ )
            (?<float> (?&digit)+ (?: \. (?&digit)* )? ))
    }gc;
    [num => $1]
}

sub variable {
    my @r = m{ \G \s* \[ ([^]]+) \]
               (?: \[ ([^]]+) \] )?
    }gc or return;
    # TODO:ask
    # what if variable in the pair ?
    # like reason=[context->custom-reason] ?
    length $r[1]
    ? [pair   => \@r    ]
    : [scalar => $r[0]  ]
}

sub pair {
    /\G \s* ( [a-zA-Z0-9_.-]+ )
        \s* \= \' ([^']+) \'  /gc or return;
    [pair => [$1, $2 ] ]
}

sub qstring {
    /\G \s* \' ([^']+) \'  /gc or return;
    [qstring => $1 ]
}


sub bareword {
    /\G \s* ( [a-zA-Z0-9_=.-]+ ) /gc or return;
    [bareword => $1]
}

sub regexp {
    m{\G \s* /((?:\\.|[^/])+)/  }gc or return;
    [regexp => $1 ]
}

sub fname { /\G \s* ([a-zA-Z0-9_-]+) \s* \( /gc or return; $1 }

sub parameters {
    my @p;
    do {
        push @p,
        num
        || qstring
        || pair
        || variable
        || regexp
        || fcall
        || bareword
    } while (/ \G \s* , \s* /gc);
    @p ? \@p : ()
}

sub fcall {
    /\G \s* ! /gc and return [qw( fcall not ), [fcall] ];
    my @fcall = ( fcall => (fname or return) );
    /\G \s* \) /gc and return \@fcall;
    my $p= parameters
        or fatal "function argument expected";
    /\G \s* \) /gc
        or fatal "closing parenthese expected";
    [ @fcall, $p ]
}

sub context {
    my @c;
    do { push @c, $1 if /\G \s* ([a-zA-Z0-9_-]+)  /gc }
        while (/ \G \s* , \s* /gc);
    @c ? \@c : ()
}

sub actions {
    /\G \s* -> \s* /gc or die;
    parameters or die;
}

sub comments {
    /\G (?: \s* \# \N* \n (?: \s* \n)* )+ /gc
}
sub meta     {
    my @m;
    do { comments } while (
            /\G \s* ([.a-zA-Z0-9_-]+)  \s+ (\N+) /gc
            && push @m, [$1 => $2]
    );
    @m ? \@m : ()
}

sub ws { /\G \s*/cg }

sub scenario {

    my @r;
    my $meta = meta;
    while (1) {
        comments;
        my $condition = fcall or last;
        my $context = context or die YAML::Dump {remains =>  /\G(.*)/};
        my $actions = actions or die YAML::Dump {remains =>  /\G(.*)/};
        push @r , [ $condition, $context, $actions ];
    }
    comments; ws;
    # TODO:open remaining data isn't my bizness ?
    #
    # my $rem= [pos,remains];
    # for my $r ($$rem[1]) {
    #     defined $r and length $r and fatal {'remaining data' => ;
    # }

    @r or fatal "no rule found";
    @r ? ( $meta => \@r ) : ();
}

sub parse {
    local %STATE;
    my $rule        = shift;
    @_ and local $_ = shift;
    #   $STATE{ast} =
    my @r = $rule->();
    STOP_PARSING:
    @r and $STATE{data} = \@r;
    \%STATE;
}

1;
