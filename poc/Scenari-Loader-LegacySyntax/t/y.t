use Test::More;
use Regexp::Grammars;
use strict;
use warnings;
use YAML;

qr{ <grammar:experiment>
    <rule:meta> <key=(.)> <value=(.)>
    # <rule:meta> ^ <key> <value> $
    #     <rule:key> [^#\s\n]+
    #     <rule:value> \N+
}x;

print qr{
    <extends:experiment>
    <meta>
}x;

"hello this is it" =~ m{
    <extends:experiment>
    <meta>
}x and print YAML::Dump {%+};


__END__

use YAML;

my @patterns = (
    [ '<extends:experiment><meta>' =>
        [ [ 'wow this is a value ' => { meta => {
                key   => 'wow',
                value => 'this is a value'
            } } ],
        ],
    ],
);

for ( @patterns ) {
    my ( $p, $tests ) = @$_;
    $p = qr{$p}x;
    for my $t ( @$tests ) {
        my ( $from, $expected ) = @$t;
        if ( ok +( $from =~ /$p/ ), "$p match" ) {
            my $got = {%+};
            is_deeply $got, $expected, "$p returns"
                or note YAML::Dump $got;
        } else {
            ok 0, "$p can't return";
        }
    }
}


done_testing;



# ok +($/{meta}{key} eq 'key')
#     => "key stored";
# ok +( $/{meta}{value} eq 'this is a value' )
#     => "value stored";
__END__
