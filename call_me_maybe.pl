use Sympatic -oo;
use Test::More;

=head1 call me maybe

so you think you know how to call a function in perl5?

=head1 it makes sense (left to right)

since postfix references was introduced, those are equivalent

    @$f
    @{ $f }
    $f->@*

and actually all of them have some pro/cons.

    * the first is short and readable
    * the second is nice to add defaults

        @{ $f //= [1..5] }

    * the third is nice to preserve reading sense

        @{ Building->new->floors }
        Building->new->floors->@*

About the Arrow Operator, it can be used to call an arbitrary subroutine on an
object without monkey patch the class.

=cut

    package Building {
        use Sympatic;
        has qw( floors is rw )
        , default => sub {[1..6]}
    }

    my $ls_floors = fun ($f) {
        say "floors: "
        , join ','
        , @$f # also $f->@*
    };

    Building->new->floors->$ls_floors;

=back

=cut


# @_ are just aliases
# $_ in for/map loops are aliases
# & shares @_
# -> calls can
# it implies both have side effects
# prototypes can avoid paraenthesis
# +() can avoid paraenthesis

sub levelup { $_+=3 for @_ }
my ( $a , $b, $c ) = 1..3;
levelup $a , $b, $c ;
is_deeply [ $a , $b, $c ]
    , [4..6]
    , "this is all about side effects";

