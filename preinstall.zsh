: <<\=cut

=head1 usage

this script autoconfigure sympa to be installed just behind itself.

=head1 example

so let's say you are in the directory containing this file, the standard usage
is

    git clone git@framagit.org:sympa/sympa.git 7
    cd 7
    zsh ../preinstall.zsh

now you have a C<$PWD/install> with an installed sympa in it.

=cut

() {
    setopt localoptions errexit
    local prefix=$1/install
    cd $1/src
    mkdir -p $prefix
    autoreconf -i
    local conf=(
      --enable-fhs
      --prefix=$prefix
      --with-user=${USER:-$( id -un )}
      --with-group=${GROUP:-$( id -gn )}
      --with-initdir=$prefix/init
      --sysconfdir=$prefix/sysconf
      --sysconfdir=$prefix/sysconf
      --mandir=$prefix/man1
      --without-smrshdir
      --with-defaultdir=$prefix/defaults
    )
    ./configure $conf
} $0:h:A
