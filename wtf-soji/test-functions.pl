use Sympatic;
use Test::More;
use Types::Standard qw< InstanceOf Str ArrayRef >;

sub Sympa::get_address { qw( jdoe example.com ) }

sub soji {
    my $format  = shift;
    my $keys    = shift;
    my %options = @_;

    if (exists $options{context}) {
        my $context = $options{context};
        if (ref $context eq 'Sympa::List') {
            @options{qw(localpart domainpart)} =
                split /\@/, Sympa::get_address($context);
        } else {
            $options{domainpart} = $context;
        }
    }

    $format =~ s/(%%|%[-#+.\d ]*[l]*\w)/$1 eq '%%' ? '%%' : '%s'/eg;


    my @args =
        map {
        if (exists $options{$_} and defined $options{$_}) {
            my $val = $options{$_};
            $val =~ s/([^0-9A-Za-z\x80-\xFF])/\\$1/g;
            $val;
        } else {
            '*';
        }
        } map {
        lc $_
        } @{$keys || []};
    my $pattern = sprintf $format, @args;
    $pattern =~ s/[*][*]+/*/g;

    # Eliminate patterns contains only punctuations:
    # ',', '.', '_', wildcard etc.
    
    return ($pattern =~ /[0-9A-Za-z\x80-\xFF]|\\[^0-9A-Za-z\x80-\xFF]/)
        ? $pattern
        : undef;

}


# Todo: $keys should be non-empty array ?
fun marc
    ( Str           $format
    , ArrayRef[Str] $keys = []
    , (Str|InstanceOf['Sympa::List']) :$context = ''
    , %a
) {

    $context and
        @a{qw( localpart domainpart )} =
            ref $context
            ? ( Sympa::get_address $context )
            : ( $a{localpart}, $context );

    $format =~ s{ % [-#+.\d ]* [l]* \w }{%s}gx;

    my $pattern =
        sprintf $format, map {
            defined
            ? s/[^0-9A-Za-z\x80-\xFF]/\\$&/gr
            : '* '
        } @a{ map lc, @$keys };

    $pattern;

    # Eliminate patterns contains only punctuations:
    # ',', '.', '_', wildcard etc.
    # $pattern =~ /[0-9A-Za-z\x80-\xFF]|\\[^0-9A-Za-z\x80-\xFF]/
    #     ? $pattern
    #     : undef;
    # so why not
    # $pattern =~ /[^,. ]/ ? undef : $pattern;
}

map {
    my ( $soji, $marc ) = map {
        (soji @$_ ),
        (marc @$_ )
    } $_;
    my ($fmt, $list) = @$_;
    is $soji, $marc, "soji and marc agreed for $fmt ${\join ',', @$list} ";
    note "soji $soji";
    note "marc $marc";
} ( [ '%s.%s.%d.%f.%s@%s_%s,%ld,%d/%s' =>
        [qw(
            priority packet_priority
            date time localpart domainpart
            tag pid rand serial)]
    ]
);

done_testing;

