use Sympatic;
use Test::More;

map {
    my ( $soji, $marc ) = map {
        s/(%%|%[-#+.\d ]*[l]*\w)/$1 eq '%%' ? '%%' : '%s'/reg
        , s{ % [-#+.\d ]* [l]* \w }
           {%s}rgx
    } $_;
    is $soji, $marc, qq(soji and marc agreed for "$_" to become "$soji")
        or note "marc thinks: $marc";
}   '%s.%s.%d.%f.%s@%s_%s,%ld,%d/%s'
    , '%%%s'
    , '%s%%s'
    , '%-[]ld%%s'
    ;

done_testing;
