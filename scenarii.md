---
vim: et ts=2 sts=2 sw=2
title: revamp scenarii
keywords: sympa scenarii perl config vm
author: Marc Chantreux <marc.chantreux@renater.fr>
---

## problems with current scenarii

there are problems with current scenaries:

* we can do so much more by adding a runtime system and some control statements
* they can't be authored or edited with a UI
* they can't be extended easily so we can't create a simple ecosystem over it

## proposed solution

* write a simple confvm that can run a list of instructions
(TODO: describe more).
* write any programs/modules that transpile whatever we want into a list
  perl can handle.

### example

these instructions

  any context smtp dkim smime md5
  goto 4                       # if true
  exit                         # if false
  listname                     # push dans $stack
  sender                       # push dans $stack
  .is_editor?                  # appelle $stack->[0]->[1]
  accept                       # is true
  reject reason='send_editor'  # if false

can be writen this way

(when (any context smtp dkim smime md5)
  (when (sender.is_editor? (listname))
    (accept)
    (reject :reason send_editor)))

also this way

  is_editor([listname],[sender]) smtp,dkim,smime,md5     -> do_it
  true()  smtp,dkim,smime,md5     -> reject(reject reason='send_editor')

and finally the JSON/YAML (so we could have a GUI to edit scenarii)
(this could be a very attractive project for a summer of code/winter is coding)

  [ ["when",
    ["any","context","smtp","dkim","smime","md5"]
    , ["when",["sender.is_editor?",["listname"]]
    , ["accept"],["reject",":reason","send_editor"]]]]

