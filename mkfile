MKSHELL=zsh

<| "zsh -c 'set -- *.md(N) ; print pages = pages/${^@%.md}.html'"
pandoc= pandoc -f markdown
template= template/page.html

all:V: $pages
    rsync -r static/ tireli.pouet pages

$pages: $template

%.html: %.pug
    pug $stem.pug > $stem.html

pages/%.html: %.md
    pandoc -f markdown --template $template -s -o $target $stem.md
