---
vim: et ts=4 sts=4 sw=4
title: Personal sympa workspace
keywords: sympa perl code
author: Marc Chantreux <marc.chantreux@renater.fr>
---

those are my notes about the sympa7 refactoring.

goto [log](log.md) for details.

# todo

## right now

* debian packages
    * debian repos
        * fast ci
            * fix sympatic Class::Load
* vm

## document the way to deal with sympa7 branch

* cherry picking from sympa-6.2 in reconciliation/date+%F branch
* refactor in refactor/ branch

# goals

respect the ideas of the 20th birthday hackathon

* be community friendly again
* modern codebase
    * readable : Sympatic based code
    * trustable: tests suites everywhere
    * be reusable: every component should be used on its own
    * hackable:
      * remove weird useless sympa hacks
        * remove SYMPALIB
        * remove die handlers in core libs
        * remove uid/gid handling
        * get grid of autoconf ?
        * remove monharc ASAP
      * make it cpan friendly (non-root by default)

# index

* [tp/](tp/) some notes about the renater tp
* [wtf-soji/](wtf-soji/) some tests written
  to understand a weird soji regex.
* [src](src) the home of sympa7
* [preinstall.zsh](preinstall.zsh)
  a script to install sympa in [install/](install/).
* [devtools/sympa-deps-control](devtools/sympa-deps-control) a debian equivs control file to install sympa deps easily

# confortable sympa hacking

those are ideas, make them yours.

## environment setup

those examples assume that those aliases are set

    # ~/.zshenv
    alias g=git
    alias v=vim
    alias z=zsh

    # git aliases
    s     =  status --short
    co    =  checkout
    br    =  branch
    bra   =  branch -a
    ci    =  commit
    rem   =  remote
    graph =  log --graph --oneline --decorate

## tips

## install deps

if you're under debian (tested on buster), the fastest way is to follow the
instructions writen in the comments of
[the equivs file](devtools/sympa-deps-control).

on any other platform, use

    cpanm --with-develop --with-feature=sympa7 --installdeps

## navigate in the code

you can navigate through modules work, PERL5LIB
should be up to date. if you use sympa6, you also
may need to setup SYMPALIB.

    # in ~/.zshrc

    workon/sympa () perl5lib=(
        /usr/share/mhonarc(/N)
        src/lib(/N)
        $perl5lib
    )

    # from shell

    workon/sympa
    cpanm --installdeps --with-feature=sympa7 .

    # if you have ctags installed
    # :h :tag
    ctags -R

## shorten the path to sympa repos

configure a shortener

    g config --global 'url.git@github.com:sympa-community/.insteadOf' sympa:

so

    g clone sympa:sympa
    cd sympa

## create a basic Sympa::Constants

this shouldn't be required during the tests

  automake --add-missing
  autoreconf
  ./configure
  make

## installing perl deps (debian)

this [debian control file](sympa-deps-control)
was created by removing the server components
from the official sympa debian package. to use it

  equivs-build devtools sympa-deps-control
  apt install ./*deb

now you have a sympa-deps package installed so

  aptitude search '~Ddepends:libmime-encwords-perl'

  p  sympa - gestionnaire moderne de listes de diffusion
  i  sympa-deps - Modern mailing list manager

voilà :)

# js tips

## logging

computed property

    wow (who)
        => console.log(`greetings ${who.name} aged of ${who.age}`
    wow ({name,age})
        => console.log(`greetings ${name} aged of ${age}`

    foo = {"name":"foo", "age":  18 };
    wow(foo)



computed property names and inline CSS in console

    foo = {"name":"foo", "age":  18 };
    bar = {"name":"bar", "age":  18 };

    console.log('%c critital','color: red;')
    console.log({ foo, bar })
    console.table([foo,bar])

timers

    console.time('looper')
    // do stuff
    console.timeEnd('looper')

stack trace

    console.trace('the name of the trace')

# PEG in sympa

PEGEX était mon choix premier (pour de multiples raisons et notament
celle de pouvoir utiliser les grammaires coté client en JS) mais zild
s'installe difficilement et ne fonctionne pas. c'est l'impression que
j'ai en gros de tout l'écosysteme qu'a créé ingy: c'est super
intéressant mais ca ne marche pas (en tout cas pas de base).

A ce stade du développement, nous allons eviter de la maintenance
supplémentaire, je vais utiliser Regexp::Grammars.

# Stages

## Automator

le nom doit etre changé (c apple je pense).

bosser sur un composant web intégrable dans une application
qui permette de génerer un IR json pour DataVM.

## acmeic ecosystem

remonter et corriger les problemes empechant le déploiement
et d'usage de l'ecosysteme d'acmeism.org en perl et js.

